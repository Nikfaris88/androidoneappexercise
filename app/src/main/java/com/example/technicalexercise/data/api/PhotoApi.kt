package com.example.technicalexercise.data.api

import com.example.technicalexercise.data.api.model.Photo
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface PhotoApi {

    @FormUrlEncoded
    @POST(ApiConstants.ENDPOINT_REST)
    suspend fun searchPhotos(
        @Field("api_key") apiKey: String,
        @Field("method") method: String,
        @Field("tags") tags: String,
        @Field("format") format: String,
        @Field("nojsoncallback") nojsoncallback: Boolean,
        @Field("extras") extras: String,
        @Field("extras") extras_urlsq: String,
        @Field("extras") extras_urlm: String,
        @Field("per_page") perPage: Int,
        @Field("page") page: Int,
    ): Photo
}