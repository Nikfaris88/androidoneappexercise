package com.example.technicalexercise.data.api.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Photo(
    @Json(name = "photos")
    val photos: Photos?,
    @Json(name = "stat")
    val stat: String
)

data class Photos(
    @Json(name = "page")
    val page: Int? = 1,
    @Json(name = "pages")
    val pages: Int? = 470,
    @Json(name = "perpage")
    val perpage: Int? = 20,
    @Json(name = "photo")
    val photo: List<PhotoX> = emptyList(),
    @Json(name = "total")
    val total: Int? = 9388
)

data class PhotoX(
    @Json(name = "farm")
    val farm: Int,
    @Json(name = "height_m")
    val height_m: Int,
    @Json(name = "id")
    val id: String,
    @Json(name = "isfamily")
    val isfamily: Int,
    @Json(name = "isfriend")
    val isfriend: Int,
    @Json(name = "ispublic")
    val ispublic: Int,
    @Json(name = "owner")
    val owner: String,
    @Json(name = "secret")
    val secret: String,
    @Json(name = "server")
    val server: String,
    @Json(name = "title")
    val title: String,
    @Json(name = "url_m")
    val url_m: String,
    @Json(name = "width_m")
    val width_m: Int
)