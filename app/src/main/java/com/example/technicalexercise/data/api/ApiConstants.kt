package com.example.technicalexercise.data.api

object ApiConstants {
    const val BASE_URL = "https://api.flickr.com/services/"
    const val ENDPOINT_REST = "rest?"
}