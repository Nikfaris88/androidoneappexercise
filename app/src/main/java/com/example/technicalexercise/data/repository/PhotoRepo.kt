package com.example.technicalexercise.data.repository

import android.util.Log
import com.example.technicalexercise.data.api.PhotoApi
import com.example.technicalexercise.data.api.model.Photo
import javax.inject.Inject

class PhotoRepo @Inject constructor(
    private val api: PhotoApi
) {
    suspend fun searchPhotos(
        apiKey: String,
        method: String,
        tags: String,
        format: String,
        nojsoncallback: Boolean,
        extras: String,
        extras_urlsq: String,
        extras_urlm: String,
        per_page: Int,
        page: Int
    ): Photo {
        val photos = api.searchPhotos(
            apiKey,
            method,
            tags,
            format,
            nojsoncallback,
            extras,
            extras_urlsq,
            extras_urlm,
            per_page,
            page
        )
        Log.d("TAG", "photos: $photos")
        return photos
    }
}