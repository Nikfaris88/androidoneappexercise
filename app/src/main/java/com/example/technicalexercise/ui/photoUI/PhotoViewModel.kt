package com.example.technicalexercise.ui.photoUI

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.technicalexercise.data.api.model.PhotoX
import com.example.technicalexercise.data.repository.PhotoRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotoViewModel @Inject constructor(
    private val photoRepo: PhotoRepo
) : ViewModel() {
    private val _state = MutableStateFlow(emptyList<PhotoX>())
    val state: StateFlow<List<PhotoX>>
        get() = _state

    private val apiKey = "5b818bd21b2232dfa97558fc78ac94a7"
    private val method = "flickr.photos.search"
    var tags = "Electrolux"
    private val format = "json"
    private val nojsoncallback = true
    private val extras = "media"
    private val extras_urlsq = "url_sq"
    private val extras_urlm = "url_m"
    private val per_page = 20
    var page = 1

    init {
        getPhotos()
    }

    fun getPhotos() {
        viewModelScope.launch {
            val photos = photoRepo.searchPhotos(
                apiKey,
                method,
                tags,
                format,
                nojsoncallback,
                extras,
                extras_urlsq,
                extras_urlm,
                per_page,
                page
            )
            if (photos.photos != null) {
                _state.value = photos.photos.photo
            }
            Log.d("TAG", "statevalue ${photos}")
        }
    }
}