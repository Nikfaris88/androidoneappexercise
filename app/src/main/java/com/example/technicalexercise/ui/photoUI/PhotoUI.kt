@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.technicalexercise.ui.photoUI

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.technicalexercise.data.api.model.PhotoX


@Composable
fun PhotoScreen(viewModel: PhotoViewModel = viewModel()) {
    val state by viewModel.state.collectAsState()

    Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {

        LazyColumn {
            if (state.isEmpty()) {
                item {

                    CircularProgressIndicator(
                        modifier = Modifier.wrapContentSize(align = Alignment.Center)
                    )
                }
            }
            items(state) { PhotoX ->
                PhotoCard(
                    photoX = PhotoX
                )
            }

        }

    }

}

@Composable
fun PhotoCard(photoX: PhotoX) {

    var selectedItem by remember { mutableStateOf<PhotoX?>(null) }
    val imagePainter = rememberAsyncImagePainter(model = photoX.url_m)
    val customCardElevation = CardDefaults.cardElevation(
        defaultElevation = 8.dp, pressedElevation = 2.dp, focusedElevation = 4.dp
    )

    Card(shape = MaterialTheme.shapes.medium,
        modifier = Modifier
            .padding(16.dp)
            .clickable {
                selectedItem = photoX
            }
            .alpha(
                if (selectedItem != null)
                    0.7f
                else
                    1.0f

            ),
        elevation = customCardElevation) {
        Box {
            Image(
                painter = imagePainter,
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp),
                contentScale = ContentScale.FillBounds
            )

            Surface(modifier = Modifier.align(Alignment.BottomCenter)) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(4.dp)
                ) {
                    Text(text = "Title: ${photoX.title}")
                }
            }
        }
    }
}

@Composable
fun SearchView(state: MutableState<TextFieldValue>, viewModel: PhotoViewModel = viewModel()) {
    TextField(
        value = state.value,
        onValueChange = { value ->
            state.value = value
            viewModel.tags = state.value.text
            viewModel.getPhotos()
        },
        modifier = Modifier.fillMaxWidth(),
        textStyle = TextStyle(color = Color.Black, fontSize = 18.sp),
        leadingIcon = {
            Icon(
                Icons.Default.Search,
                contentDescription = "",
                modifier = Modifier
                    .padding(15.dp)
                    .size(24.dp)
            )
        },
        trailingIcon = {
            if (state.value != TextFieldValue("")) {
                IconButton(onClick = {
                    state.value =
                        TextFieldValue("") // Remove text from TextField when you press the 'X' icon
                }) {
                    Icon(
                        Icons.Default.Close,
                        contentDescription = "",
                        modifier = Modifier
                            .padding(15.dp)
                            .size(24.dp)
                    )
                }
            }
        },
        singleLine = true,
        shape = RectangleShape, // The TextFiled has rounded corners top left and right by default
        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.Black,
            cursorColor = Color.Black,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        )
    )
}